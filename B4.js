var kiemTraTamGiac = function () {
  var e1 = document.getElementById("edge1").value * 1;
  var e2 = document.getElementById("edge2").value * 1;
  var e3 = document.getElementById("edge3").value * 1;
  var isTriangle = e1 + e2 > e3 && e1 + e3 > e2 && e2 + e3 > e1;
  var isVuong =
    isTriangle &&
    (e1 * e1 + e2 * e2 == e3 * e3 ||
      e1 * e1 + e3 * e3 == e2 * e2 ||
      e3 * e3 + e2 * e2 == e1 * e1);
  var loaiTamGiac = !isTriangle
    ? "Invalid Triangle"
    : isVuong
    ? "Tam giác vuông"
    : e1 == e2 && e2 == e3 && e1 == e3
    ? "Tam giác đều"
    : e1 != e2 && e2 != e3 && e1 != e3
    ? "Tam giác thường"
    : "Tam giác cân";
  document.getElementById("tamGiac").innerHTML = `${loaiTamGiac}`;
};
